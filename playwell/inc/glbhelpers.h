#ifndef GLBHELPERS_H
#define GLBHELPERS_H

//Uncomment the following line to print debug info to the UART
#define DEBUG_INFO

//Uncomment one of the following two lines depending on the board hardware
//#define PHYSICAL_BUTTONS_TILES // For tiles with physical buttons
#define NFC_TAG_TILES    // For tiles with RFID readers and pieces with tags

/*****************************/
#ifdef PHYSICAL_BUTTONS_TILES

#ifdef NFC_TAG_TILES
#error Need to define ONLY 1 board hardware option in glbhelpers.h file
#endif

#else

#ifndef NFC_TAG_TILES
#error Need to define board hardware option in glbhelpers.h file
#endif

#endif
/*****************************/

/* Global Function Declarations*/

// --> From C based functions

extern "C"
{
  // Hardware Module
  void hardwareInit (void);
  int pageUpdate (void);
  int batteryUpdate (void);
  void disableBLEandSleep (void);
  void setup(void);

  // Buttons Module
  void buttonsInit (void);
  int waitButtonPress (void);
  // Touch Module
  void touchInit (void);
  int waitTouchPress (void);
}

// --> From C++ functions

// Bluetooth Module
void bleInit (void);
void sendBLData (int button, int page, int battery);
bool statusBL (void);
void setBLEcmdMode (void);
void setBLEname (String name);

//Timer module
void timerSet (int minutes);
void timerStop (void);

#endif /* GLBHELPERS_H */

/* End of file */
