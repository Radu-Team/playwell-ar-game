#ifndef BOARD_H
#define BOARD_H

#include <Arduino.h>

#define ACTIVE_TILES 15

#define TILE_THR_1 100
#define TILE_THR_2 100
#define TILE_THR_3 100
#define TILE_THR_4 100

int activeTilesPins[ACTIVE_TILES] = {A0, A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, A11, A12, A13, A14};
int boardStatus[ACTIVE_TILES]     = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
int changedTiles[ACTIVE_TILES]    = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

#endif /* BOARD_H */

/* End of file */