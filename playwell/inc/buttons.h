#ifndef BUTTONS_H
#define BUTTONS_H

#include <Arduino.h>

  struct s_buttons
  {
    struct s_player
    {
      const int one;
      const int two;
      const int three;
      const int four;
    } player;
    
    struct s_pin
    {
      const int b1;
      const int b2;
      const int b3;
      const int b4;
    } pin;

    struct s_value
    {
      int b1;
      int b2;
      int b3;
      int b4;
    } value;
  };

  struct s_buttons buttons =
  {
    .player = {2, 3, 18, 19},
    .pin = {4, 5, 6, 7},
    .value = {0, 0, 0, 0}
  };

#endif /* BUTTONS_H */

/* End of file */