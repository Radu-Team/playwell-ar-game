/****************************/
// Project: Playwell
// Module Name: board
//
// Desription: This module contains the necessary functions to read
//  the board status and recognise which of the active tiles are activated
//  and which player is on each.
//
// Authors: George (Radu's team)
//
/****************************/

#include "../inc/board.h"
#include "../inc/glbhelpers.h"

void boardInit (void)
{
  analogReference (DEFAULT);
  boardUpdate ();
}

bool boardUpdate (void)
{
  int newBoard[ACTIVE_TILES];
  bool changed = false;

  for (int i = 0; i < ACTIVE_TILES; ++i)
  {
    int tileValue = analogRead(activeTilesPins[i]);
    newBoard[i] = tileValueToPieceNumber (tileValue);
    if (newBoard[i] != boardStatus[i])
    {
      changed = true;
      changedTiles[i] = newBoard[i];
    }
    boardStatus[i] = newBoard[i];
  }

  return changed;
}

int tileValueToPieceNumber (int value)
{
  int pieceNumber = 0;

  if (value < TILE_THR_1)
  {
    pieceNumber = 0;
  }
  else if (value < TILE_THR_2)
  {
    pieceNumber = 1;

  }
  else if (value < TILE_THR_3)
  {
    pieceNumber = 2;
  }
  else if (value < TILE_THR_4)
  {
    pieceNumber = 4;
  }
  else
  {
    pieceNumber = 0;
  }

  return pieceNumber;
}

/* End of file */