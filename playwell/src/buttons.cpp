/****************************/
// Project: Playwell
// Module Name: buttons
//
// Desription: This modules contains the functions needed to initialize
//  and read the physicall buttons of the device (4 per player)
//
// Authors: George, (Radu's team)
//
/****************************/

#include "../inc/buttons.h"
#include "../inc/glbhelpers.h"
#include <avr/sleep.h>

/* Local function declarations */
void buttonsUpdate (void);
void p1KeyboardPressed (void);
void p2KeyboardPressed (void);

extern bool sleepReady;

int buttonPressed = 0;

//                       [0, 1, 2, 3, 4, 5, 6, 7, 8] 
//int buttonCorrection[] = {0, 5, 4, 7, 6, 8, 2, 3, 1};

void buttonsInit (void)
{
  //Setup Button Pins
  pinMode(buttons.pin.b1, INPUT);
  pinMode(buttons.pin.b2, INPUT);
  pinMode(buttons.pin.b3, INPUT);
  pinMode(buttons.pin.b4, INPUT);

  pinMode(buttons.player.one,   INPUT_PULLUP);
  pinMode(buttons.player.two,   INPUT_PULLUP);
  pinMode(buttons.player.three, INPUT_PULLUP);
  pinMode(buttons.player.four,  INPUT_PULLUP);
}

void buttonsUpdate (void)
{
  // Read the player side input and then decide what to do next ...
  buttons.value.b1 = digitalRead(buttons.pin.b1);
  buttons.value.b2 = digitalRead(buttons.pin.b2);
  buttons.value.b3 = digitalRead(buttons.pin.b3);
  buttons.value.b4 = digitalRead(buttons.pin.b4);

  //Turn the binary representation of the buttons pressed into an actuall button number
  buttonPressed = (1*buttons.value.b1)+(2*buttons.value.b2)+(3*buttons.value.b3)+(4*buttons.value.b4);
}

int waitButtonPress (void)
{
  attachInterrupt(digitalPinToInterrupt(buttons.player.one), p1KeyboardPressed, FALLING);
  attachInterrupt(digitalPinToInterrupt(buttons.player.two), p2KeyboardPressed, FALLING);
  
#ifdef DEBUG_INFO
  Serial.println("Going to sleep");
  delay(50);
#endif

  sleepReady = true;
  set_sleep_mode(2);
  sleep_enable();
  sleep_cpu();
  sleep_disable();

  buttonPressed = buttonCorrection[buttonPressed];
  return buttonPressed;
}

void p1KeyboardPressed (void)
{
  if (sleepReady==true)
  {
    detachInterrupt(digitalPinToInterrupt(buttons.player.one));
    detachInterrupt(digitalPinToInterrupt(buttons.player.two));
    
#ifdef DEBUG_INFO
    Serial.println("Player 1 keyboard");
#endif

    pinMode(buttons.player.one, OUTPUT);
    digitalWrite(buttons.player.one, HIGH);
    buttonsUpdate();

    pinMode(buttons.player.one, INPUT_PULLUP);
    attachInterrupt(digitalPinToInterrupt(buttons.player.one), p1KeyboardPressed, FALLING);
    attachInterrupt(digitalPinToInterrupt(buttons.player.two), p2KeyboardPressed, FALLING);
    sleepReady = false;
  }
}

void p2KeyboardPressed (void)
{
  if (sleepReady == true)
  {
    detachInterrupt(digitalPinToInterrupt(buttons.player.one));
    detachInterrupt(digitalPinToInterrupt(buttons.player.two));
    
#ifdef DEBUG_INFO
    Serial.println("Player 2 keyboard");
#endif

    pinMode(buttons.player.two, OUTPUT);
    digitalWrite(buttons.player.two, HIGH);
    buttonsUpdate();
    if (buttonPressed != 0) buttonPressed += 4;
    
    pinMode(buttons.player.two, INPUT_PULLUP);
    attachInterrupt(digitalPinToInterrupt(buttons.player.one), p1KeyboardPressed, FALLING);
    attachInterrupt(digitalPinToInterrupt(buttons.player.two), p2KeyboardPressed, FALLING);
    sleepReady = false;
  }
}