/****************************/
// Project: Playwell
// Module Name: bluetooth
//
// Desription: This modules contains the functions needed to setup
//  and use the UART bluetooth module of the project as well as
//  some helpers.
//
// Authors: George (Radu's team)
//
/****************************/

#include "../inc/bluetooth.h"
#include "../inc/glbhelpers.h"

/* Local function declaration */
void error(const __FlashStringHelper*err);

void bleInit (void)
{
#ifdef DEBUG_INFO
  Serial.println(F("BleUart Initialisation procedure"));
  Serial.println(F("--------------------------------"));
  delay(50);
#endif

  /* Initialise the module */
  if ( !ble.begin(VERBOSE_MODE) )
  {
    error(F("Couldn't find module, make sure it's in CoMmanD mode & check wiring?"));
  }

#ifdef DEBUG_INFO
  Serial.println( F("Initialisation OK!") );
#endif

  if ( FACTORYRESET_ENABLE )
  {
    /* Perform a factory reset to make sure everything is in a known state */
#ifdef DEBUG_INFO
    Serial.print("Performing a factory reset: ...");
#endif

    if ( ! ble.factoryReset() ){
      error(F("Couldn't factory reset"));
    }

#ifdef DEBUG_INFO
    Serial.println( F("Reset OK!") );
#endif
  }

  /* Disable command echo from Bluefruit */
  ble.echo(false);

#ifdef DEBUG_INFO
  Serial.println("Requesting Bluefruit info: ...");
  /* Print Bluefruit information */
  ble.info();
#endif

#ifndef DEBUG_INFO
  ble.verbose(false);  // debug info is a little annoying after this point!
#endif

#ifdef DEBUG_INFO
  Serial.println( F("Switching to DATA mode!") );
#endif
  ble.setMode(BLUEFRUIT_MODE_DATA);
}

void error(const __FlashStringHelper*err) {
  Serial.println(err);
  while (1);}

void sendBLDataRaw (String data)
{
  ble.println(data);
}

void sendBLDataFormat (command cmd, int* data)
{
  // @todo inmplement protocol sending a command with data to the phone
}

bool statusBL (void)
{
  bool status = ble.isConnected();

#ifdef DEBUG_INFO
  if (status==true)
  { 
    Serial.println("BLE connected");
  }
  else
  {
    Serial.println("BLE not connected");
  }
#endif

  return status;
}

bool bleReceivedCmd (int * command)
{
  bool received = ble.available();
  int i = 0;
  while ( ble.available() )
  {
    command[i++] = ble.read();
    if (i==CMD_MAX_SIZE) break;
  }

  return received;
}

void setBLEcmdMode (void)
{
  ble.setMode(BLUEFRUIT_MODE_COMMAND);
}

void setBLEname (String name)
{
  ble.setMode(BLUEFRUIT_MODE_COMMAND);
  ble.println("AT+GAPDEVNAME="+name);
  ble.waitForOK();
  ble.setMode(BLUEFRUIT_MODE_DATA);
}

/* End of file */
